package com.travactory.recruitment.junior;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.travactory.recruitment.junior.controller.BookingsController;
import com.travactory.recruitment.junior.model.BookingPrice;
import com.travactory.recruitment.junior.service.BookingsService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookingsController.class)
public class JuniorApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BookingsService bookingService;

	@Test
	public void calculatingPriceFirstBooking() throws Exception {

		BookingPrice price = new BookingPrice(3, 123.45, 370.35);
		when(bookingService.calculateBookingPrice(1)).thenReturn(price);

		mockMvc.perform(get("/api/bookings/{id}/price", 1))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.passengers", is(3)))
				.andExpect(jsonPath("$.passengerPrice", is(123.45)))
				.andExpect(jsonPath("$.totalPrice", is(370.35)));

		verify(bookingService, times(1)).calculateBookingPrice(1);
		verifyNoMoreInteractions(bookingService);
	}
	
	@Test
	public void calculatingPriceSecondBooking() throws Exception {

		BookingPrice price = new BookingPrice(2, 276.12, 552.24);
		when(bookingService.calculateBookingPrice(2)).thenReturn(price);

		mockMvc.perform(get("/api/bookings/{id}/price", 2))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.passengers", is(2)))
				.andExpect(jsonPath("$.passengerPrice", is(276.12)))
				.andExpect(jsonPath("$.totalPrice", is(552.24)));

		verify(bookingService, times(1)).calculateBookingPrice(2);
		verifyNoMoreInteractions(bookingService);
	}

}
