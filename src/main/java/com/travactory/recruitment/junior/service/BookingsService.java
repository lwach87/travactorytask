package com.travactory.recruitment.junior.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.travactory.recruitment.junior.model.BookingPrice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.travactory.recruitment.junior.model.Booking;
import com.travactory.recruitment.junior.model.FlightDuration;
import com.travactory.recruitment.junior.repository.BookingsRepository;

@Service
public class BookingsService {

	@Autowired
	private BookingsRepository bookingsRepository;

	public BookingsService(final BookingsRepository bookingsRepository) {
		this.bookingsRepository = bookingsRepository;
	}

	public Booking getBookingById(final Integer id) {
		return bookingsRepository.findOne(id);
	}

	public List<Booking> getAllBookings() {
		return new ArrayList<>(bookingsRepository.findAll());
	}

	public Booking addBooking(Booking booking) {
		return bookingsRepository.save(booking);
	}

	public FlightDuration calculateFlightDuration(final Integer bookingId) {
		final Booking booking = getBookingById(bookingId);

		final OffsetDateTime offsetDeparture = getOffsetDateTime(booking.getDepartureDate(),
				booking.getDeparture().getGmtTimeZoneOffset());

		final OffsetDateTime offsetArrival = getOffsetDateTime(booking.getArrivalDate(),
				booking.getDestination().getGmtTimeZoneOffset());

		final Duration duration = Duration.between(offsetDeparture, offsetArrival);

		return new FlightDuration(duration.toDays(), duration.toHours() % 24, duration.toMinutes() % 60);
	}

	private OffsetDateTime getOffsetDateTime(Date date, Integer offset) {

		final String zoneOffset = (offset > 0) ? "+" + String.valueOf(offset) : String.valueOf(offset);

		return OffsetDateTime.of(LocalDateTime.ofInstant(date.toInstant(), ZoneId.of("CET")),
				ZoneOffset.of(zoneOffset));
	}

	public BookingPrice calculateBookingPrice(final Integer bookingId) {
		final Booking booking = getBookingById(bookingId);

		return new BookingPrice(booking.getPassengers(), booking.getPassengerPrice(),
				booking.getPassengers() * booking.getPassengerPrice());
	}
}
