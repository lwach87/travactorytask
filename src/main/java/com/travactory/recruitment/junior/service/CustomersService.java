package com.travactory.recruitment.junior.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.travactory.recruitment.junior.model.Customer;
import com.travactory.recruitment.junior.repository.CustomersRepository;

@Service
public class CustomersService {

	@Autowired
	private CustomersRepository customersRepository;

	public Customer getCustomerById(final Integer id) {
		return customersRepository.findOne(id);
	}

	public List<Customer> getAllCustomers() {
		return new ArrayList<>(customersRepository.findAll());
	}

}
