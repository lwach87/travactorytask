package com.travactory.recruitment.junior.controller;

import com.travactory.recruitment.junior.model.BookingPrice;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.travactory.recruitment.junior.model.Booking;
import com.travactory.recruitment.junior.model.FlightDuration;
import com.travactory.recruitment.junior.service.BookingsService;

@RestController
@RequestMapping("/api/bookings")
public class BookingsController {

	@Autowired
	private BookingsService bookingsService;

	public BookingsController(final BookingsService bookingsService) {
		this.bookingsService = bookingsService;
	}

	@GetMapping("{id}")
	public ResponseEntity<?> getBookingById(@PathVariable(name = "id") final Integer id) {
		final Booking booking = bookingsService.getBookingById(id);
		return ResponseEntity.ok(booking);
	}

	@GetMapping("")
	public ResponseEntity<?> getAllBookings() {
		final List<Booking> bookings = bookingsService.getAllBookings();
		return ResponseEntity.ok(bookings);
	}

	@PostMapping(value = "")
	public void addBooking(@RequestBody Booking booking) throws IOException {

		/* Add new booking with hard-coded id values for customer, airport departure and destination
		 * instead of nested objects
		
		booking.setDeparture(new Airport(1, "", "", null));
		booking.setDestination(new Airport(3, "", "", null));
		booking.setCustomer(new Customer(3, "", "", "", "", null));
		*/
		bookingsService.addBooking(booking);
	}

	@GetMapping("{id}/duration")
	public ResponseEntity<?> calculateDuration(@PathVariable(name = "id") final Integer id) {
		final FlightDuration flightDuration = bookingsService.calculateFlightDuration(id);
		return ResponseEntity.ok(flightDuration);
	}

	@GetMapping("{id}/price")
	public ResponseEntity<?> calculatePrice(@PathVariable(name = "id") final Integer id) {
		final BookingPrice bookingPrice = bookingsService.calculateBookingPrice(id);
		return ResponseEntity.ok(bookingPrice);
	}
}
