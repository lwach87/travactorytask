package com.travactory.recruitment.junior.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.travactory.recruitment.junior.model.Customer;
import com.travactory.recruitment.junior.service.CustomersService;

@RestController
@RequestMapping("/api/customers")
public class CustomersController {

	@Autowired
	private CustomersService customersService;

	@GetMapping("{id}")
	public ResponseEntity<?> getCustomerById(@PathVariable(name = "id") final Integer id) {
		final Customer customer = customersService.getCustomerById(id);
		return ResponseEntity.ok(customer);
	}

	@GetMapping("")
	public ResponseEntity<?> getAllCustomers() {
		final List<Customer> customers = customersService.getAllCustomers();
		return ResponseEntity.ok(customers);
	}
}
