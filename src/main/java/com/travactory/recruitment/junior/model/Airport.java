package com.travactory.recruitment.junior.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "airports")
public class Airport {
	@Id
	private Integer id;
	/** Unique IATA airport code */
	private String code;
	/** Representative name */
	private String name;
	/** Time offset from GMT time zone (in hours) */
	private Integer gmtTimeZoneOffset;

	public Airport() {
	}

	public Airport(Integer id, String code, String name, Integer gmtTimeZoneOffset) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.gmtTimeZoneOffset = gmtTimeZoneOffset;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public Integer getGmtTimeZoneOffset() {
		return gmtTimeZoneOffset;
	}
}
