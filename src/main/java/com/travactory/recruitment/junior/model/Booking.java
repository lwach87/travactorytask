package com.travactory.recruitment.junior.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "bookings")
public class Booking {

	@Id
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "departure_airport_id")
	private Airport departure;
	@ManyToOne
	@JoinColumn(name = "destination_airport_id")
	private Airport destination;
	private Date departureDate;
	private Date arrivalDate;
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	private Double passengerPrice;
	private Integer passengers;
	private String classType;

	public Booking() {
	}

	/* Constructor with departureId, destinationId and customerId instead of nested airport and customer objects
	
	public Booking(Integer id, Integer departureId, Integer destinationId, Date departureDate, Date arrivalDate,
			Integer customerId, Double passengerPrice, Integer passengers, String classType) {
		this.id = id;
		this.departure = new Airport(departureId, "", "", null);
		this.destination = new Airport(destinationId, "", "", null);
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.customer = new Customer(customerId, "", "", "", "", null);
		this.passengerPrice = passengerPrice;
		this.passengers = passengers;
		this.classType = classType;
	}
	
	*/
	
	public Booking(Integer id, Airport departure, Airport destination, Date departureDate, Date arrivalDate,
			Customer customer, Double passengerPrice, Integer passengers, String classType) {
		this.id = id;
		this.departure = departure;
		this.destination = destination;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.customer = customer;
		this.passengerPrice = passengerPrice;
		this.passengers = passengers;
		this.classType = classType;
	}

	public Integer getId() {
		return id;
	}

	public Airport getDeparture() {
		return departure;
	}

	public void setDeparture(Airport departure) {
		this.departure = departure;
	}

	public Airport getDestination() {
		return destination;
	}

	public void setDestination(Airport destination) {
		this.destination = destination;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Double getPassengerPrice() {
		return passengerPrice;
	}

	public Integer getPassengers() {
		return passengers;
	}

	public String getClassType() {
		String classTypeFullName;
		switch (classType) {
		case "F":
			classTypeFullName = "First";
			break;
		case "E":
			classTypeFullName = "Economy";
			break;
		case "B":
			classTypeFullName = "Business";
			break;
		default:
			classTypeFullName = "Unknown class " + classType;
		}
		return classTypeFullName;
	}
}
